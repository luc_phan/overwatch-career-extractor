from setuptools import setup

setup(
    name='overwatch-career-extractor',
    version='0.0.1',
    packages=['overwatchcareerextractor'],
    url='',
    license='',
    author='luc2',
    author_email='',
    description='',
    entry_points={'console_scripts': ['oce = overwatchcareerextractor.command:main']},
)

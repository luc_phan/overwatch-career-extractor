import click
from pywinauto import application
# import time
from PIL import Image, ImageDraw
# import numpy as np
import pytesseract
import os


@click.command()
def main():
    click.echo("Click test!")
    # app = application.Application().start("notepad.exe")
    # time.sleep(1)  # requires a delay...
    # app.Untitled_Notepad.capture_as_image().save('window.png')
    app = application.Application().connect(title='Overwatch')
    app.Overwatch.set_focus()
    app.Overwatch.capture_as_image().save('window.png')

    # image = Image.open('window.png')
    # pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files (x86)\Tesseract-OCR\tesseract.exe'
    # result = pytesseract.image_to_string(image)
    # print(result)

    im = Image.open('window.png')
    print(im.size)
    draw = ImageDraw.Draw(im)
    # draw.rectangle([(35, 5), (100, 25)], outline=(255, 0, 0))
    del draw
    cropped = im.crop((35, 5, 100, 25))
    # cropped.show()
    cropped.save('window2.png')

    # pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files (x86)\Tesseract-OCR\tesseract.exe'
    pytesseract.pytesseract.tesseract_cmd = r'C:\Programs\tesseract-Win64\tesseract.exe'
    os.environ['TESSDATA_PREFIX'] = r'C:\Programs\tesseract-Win64'
    result = pytesseract.image_to_string(cropped)
    print(result)

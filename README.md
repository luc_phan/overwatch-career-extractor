Python packages
---------------

### From the video

- https://www.youtube.com/watch?v=qQcHkMvsQwA
  * pillow
  * tesseract
  * pyautogui

### From reseaches

- argparse (already tried...)
- click (trying...)
- docopt
- clize

Take a screenshot of a window
-----------------------------

- https://stackoverflow.com/questions/30846493/python-screenshot-application-window-at-any-size
- https://stackoverflow.com/questions/3260559/how-to-get-a-window-or-fullscreen-screenshot-in-python-3k-without-pil
  * Requires `win32gui`
    - Impossible to install `win32gui` with Python 3.7: https://stackoverflow.com/questions/52806906/cant-install-win32gui

Status of this project
----------------------

Failed! Could even not recognise the word "Overwatch"... maybe an older version of tesseract... Failed too!

